/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.wordsapp

import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_DIAL
import android.net.Uri
import android.provider.AlarmClock.ACTION_SET_ALARM
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView

/**
 * Adapter for the [RecyclerView] in [DetailActivity].
 */
class WordAdapter(private val letterId: String, context: Context) :
    RecyclerView.Adapter<WordAdapter.WordViewHolder>() {

    private val filteredWords: List<String>

    init {
        // retrieve the list of words from res/values/arrays.xml
        val words = context.resources.getStringArray(R.array.words).toList()

        // .filter{...} function returns items in a collection if the conditional clause is true,
        // in this case if an item starts with the given letter, ignoring UPPERCASE or lowercase.
        // .shuffled() returns a collection that it has shuffled in place
        // .take(N) returns the first M items as a List
        // .sorted() returns a sorted version of that List
        filteredWords = words

            .filter { it.startsWith(letterId, ignoreCase = true) }
            .shuffled()
            .take(5)
            .sorted()
    }

    class WordViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val button: Button = view.findViewById(R.id.button_item)
    }

    override fun getItemCount(): Int = filteredWords.size

    /**
     * Creates new views with `R.layout.item_view` as its template
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val layout = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_view, parent, false)

        // setup custom accessibility delegate to set the text read
        layout.accessibilityDelegate = Accessibility

        return WordViewHolder(layout)
    }

    /**
     * Replaces the content of an existing view with new data.
     *
     * [Intent.ACTION_VIEW] is a generic intent that takes a [Uri].
     * Some other intent types include:
     * - [Intent.CATEGORY_APP_MAPS]: launching the maps app
     * - [Intent.CATEGORY_APP_EMAIL]: launching the email app
     * - [Intent.CATEGORY_APP_GALLERY]: launching the gallery (photos) app
     * - [ACTION_SET_ALARM]: setting an alarm in the background
     * - [ACTION_DIAL]: initiating a phone call
     *
     * Docs: [Common Intents](https://developer.android.com/guide/components/intents-common)
     */
    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val item = filteredWords[position]

        // needed to call startActivity
        val context = holder.view.context

        // set the text of the WordViewHolder
        holder.button.text = item

        // assigns an OnClickListener to the button contained in the ViewHolder
        holder.button.setOnClickListener {
            val queryUrl: Uri = Uri.parse("${DetailActivity.SEARCH_PREFIX}${item}")
            val intent = Intent(Intent.ACTION_VIEW, queryUrl)
            context.startActivity(intent)
        }
    }

    /**
     * Accessibility
     *
     * Setup custom accessibility delegate to set the text read with an accessibility service
     * Docs: [Object Declarations](https://kotlinlang.org/docs/object-declarations.html#object-declarations-overview)
     *
     * @constructor Create empty Accessibility
     */
    companion object Accessibility : View.AccessibilityDelegate() {
        override fun onInitializeAccessibilityNodeInfo(
            host: View,
            info: AccessibilityNodeInfo
        ) {
            super.onInitializeAccessibilityNodeInfo(host, info)
            val customString = host.context?.getString(R.string.look_up_word)
            val customClick =
                AccessibilityNodeInfo.AccessibilityAction(
                    AccessibilityNodeInfo.ACTION_CLICK,
                    customString
                )
            info.addAction(customClick)
        }
    }
}