/*
 * Copyright (C) 2020 The Android Open Source Project
 * https://github.com/google-developer-training/android-basics-kotlin-words-app/tree/activities
 */
package com.pietropoluzzi.wordsapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.MenuProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pietropoluzzi.wordsapp.databinding.ActivityMainBinding

/**
 * Main Activity
 *
 * ## Intents
 * An intent is an object representing some action to be performed (for example launch an activity).
 * There are two types of intents: implicit and explicit.
 * An explicit intent is highly specific, where you know the exact activity to be launched, often a screen in the app.
 * Explicit intents are used for actions or presenting screens in your own app and are responsible for the entire process.
 * Implicit intents are used for performing actions involving other apps and rely on the system to determine the end result.
 * Both implicit and explicit intents describe the request, not the actual result.
 * ---
 * A URL (Uniform Resource Locator) is a subclass of URI (Uniform Resource Identifier).
 * It starts with `https://`, `mailto:` and many other.
 * Another subclass is URN (Uniform Resource Name): an address for a phone number (starts with `tel:`).
 * ---
 * The `onSaveInstanceState()` method is a callback you use to save any data that you might need if the Activity is destroyed.
 * In the lifecycle callback diagram, it is called after the activity has been stopped.
 * It's called every time your app goes into the background.
 * If the activity is being re-created, the `onRestoreInstanceState()` callback is called after `onStart()`, also with the bundle.
 * Most of the time, you restore the activity state in `onCreate()`.
 * But because `onRestoreInstanceState()` is called after `onStart()`, if you ever need to restore some state after `onCreate()` is called, you can use `onRestoreInstanceState()`.
 *
 * Docs: [Introduction to activities](https://developer.android.com/guide/components/activities/intro-activities#kotlin)
 */
class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    // Keeps track of which LayoutManager is in use for the [RecyclerView]
    private var isLinearLayoutManager = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        recyclerView = binding.recyclerView
        // sets the LinearLayoutManager of the recyclerview
        chooseLayout()
    }

    /**
     * Sets the LayoutManager for the [RecyclerView] based on the desired orientation of the list.
     */
    private fun chooseLayout() {
        if (isLinearLayoutManager) {
            recyclerView.layoutManager = LinearLayoutManager(this)
        } else {
            recyclerView.layoutManager = GridLayoutManager(this, 4)
        }
        recyclerView.adapter = LetterAdapter()
    }

    /**
     * Set the drawable for the menu icon based on which LayoutManager is currently in use
     *
     * @param menuItem Activity's [MenuItem] instance to modify
     */
    private fun setIcon(menuItem: MenuItem?) {
        if (menuItem == null)
            return

        // an if-clause can be used on the right side of an assignment if all paths return a value.
        // the following code is equivalent to:
        // if (isLinearLayoutManager)
        //     menu.icon = ContextCompat.getDrawable(this, R.drawable.ic_grid_layout)
        // else menu.icon = ContextCompat.getDrawable(this, R.drawable.ic_linear_layout)
        menuItem.icon =
            if (isLinearLayoutManager)
                ContextCompat.getDrawable(this, R.drawable.ic_grid_layout)
            else ContextCompat.getDrawable(this, R.drawable.ic_linear_layout)
    }

    /**
     * Initializes the [Menu] to be used with the current [Activity]
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.layout_menu, menu)

        val layoutButton = menu?.findItem(R.id.action_switch_layout)
        // Calls code to set the icon based on the LinearLayoutManager of the RecyclerView
        setIcon(layoutButton)

        return true
    }

    /**
     * Determines how to handle interactions with the selected [MenuItem]
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_switch_layout -> {
                // sets isLinearLayoutManager (a Boolean) to the opposite value
                isLinearLayoutManager = !isLinearLayoutManager
                // Sets layout and icon
                chooseLayout()
                setIcon(item)

                return true
            }
            // otherwise, do nothing and use the core event handling

            // when clauses require that all possible paths be accounted for explicitly,
            // for instance both the true and false cases if the value is a Boolean,
            // or an else to catch all unhandled cases.
            else -> super.onOptionsItemSelected(item)
        }
    }
}
