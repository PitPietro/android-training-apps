/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.wordsapp.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

/**
 * constant value representing the Layout Preferences Name
 */
private const val LAYOUT_PREFERENCES_NAME = "layout_preferences"

/**
 * Create a DataStore instance using the preferencesDataStore delegate, with the Context as receiver.
 */
val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
    name = LAYOUT_PREFERENCES_NAME
)

/**
 * Create the DataStore instance at the top level of the file once, and access it through this property throughout the rest of your app.
 * This makes it easier to keep your DataStore as a singleton.
 *
 * @constructor Create empty Settings data store
 */
class SettingsDataStore(preference_datastore: DataStore<Preferences>) {
    private val IS_LINEAR_LAYOUT_MANAGER = booleanPreferencesKey("is_linear_layout_manager")

    /**
     * Preferences [DataStore] exposes the data stored in a `Flow<Preferences>` that emits every time a preference has changed.
     * You don't want to expose the entire [Preferences] object, just the [Boolean] value.
     * To do this, map the `Flow<Preferences>` and get the [Boolean] value.
     *
     * As [DataStore] reads and writes data from files, `IOExceptions` may occur when accessing the data.
     * Handle these using the [catch] operator.
     */
    val preferenceFlow: Flow<Boolean> = preference_datastore.data
        .catch {
            if (it is IOException) {
                it.printStackTrace()
                emit(emptyPreferences())
            } else {
                throw it
            }
        }
        .map { preferences ->
            // on the first run of the app, use LinearLayoutManager by default
            preferences[IS_LINEAR_LAYOUT_MANAGER] ?: true
        }

    /**
     * Preferences [DataStore] provides [edit] suspend function that updates the data in DataStore in a transactional way.
     * The function's transform parameter accepts a block of code where you can update the values as needed.
     * All of the code in the transform block is treated as a single transaction.
     * Under the hood the transaction work is moved to `Dispacter.IO`, so don't forget to make the function *suspend* when calling [edit].
     *
     * @param isLinearLayoutManager [Boolean] value that tells is the Layout Manager is linear or not
     * @param context UI component context
     */
    suspend fun saveLayoutToPreferencesStore(isLinearLayoutManager: Boolean, context: Context) {
        context.dataStore.edit { preferences ->
            preferences[IS_LINEAR_LAYOUT_MANAGER] = isLinearLayoutManager
        }
    }
}