/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.wordsapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.pietropoluzzi.wordsapp.R
import com.pietropoluzzi.wordsapp.databinding.ActivityMainBinding

/**
 * Main Activity and entry point for the app.
 *
 * Preferences [DataStore](https://developer.android.com/topic/libraries/architecture/datastore)
 * is ideal for small, simple datasets, such as storing login details, the dark mode setting, font size, and so on.
 * The DataStore is not suitable for complex datasets, such as an online grocery store inventory list, or a student database.
 *
 *
 * Using the Jetpack DataStore library you can create a simple, safe, and asynchronous API for storing data.
 * It provides two different implementations: Preferences DataStore and Proto DataStore.
 * While both Preferences and Proto DataStore allow saving data, they do it in different ways:
 * - **Preferences** DataStore accesses and stores data based on keys, without defining a schema (database model) upfront.
 * - **Proto** DataStore defines the schema using [Protocol buffers](https://protobuf.dev/).
 * It lets you persist strongly typed data.
 * Protobufs are faster, smaller, simpler, and less ambiguous than XML and other similar data formats.
 * */
class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // get the navigation host fragment from this Activity
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        // instantiate the navController using the NavHostFragment
        navController = navHostFragment.navController
        // make sure actions in the ActionBar get propagated to the NavController
        setupActionBarWithNavController(navController)
    }

    /**
     * Enables back button support.
     * Simply navigates one element up on the stack.
     */
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}
