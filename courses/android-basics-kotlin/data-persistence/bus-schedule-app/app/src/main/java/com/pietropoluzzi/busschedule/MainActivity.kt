/*
 * Copyright (C) 2023 Pietro Poluzzi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.busschedule

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.pietropoluzzi.busschedule.databinding.ActivityMainBinding

/**
 * The ViewModel class is used to store data related to an app's UI, and is also lifecycle aware, meaning that it responds to lifecycle events much like an activity or fragment does.
 * If lifecycle events such as screen rotation cause an activity or fragment to be destroyed and recreated, the associated ViewModel won't need to be recreated.
 * This is not possible with accessing a DAO class directly, so it's best practice to use ViewModel subclass to separate the responsibility of loading data from your activity or fragment.
 *
 * This application is a relatively simple and only includes two screens of mostly identical content.
 * For learning purpose, is being used a single view model class that can be used by both screens, but in a larger app, you may want to use a separate view model for each fragment.
 *
 * Navigate to: `View` > `Tool Windows` > `App Inspection` > `Database Inspector`
 *
 * Run SQL queries against the database, like:
 * ```MySQL
 * INSERT INTO schedule
 * VALUES (null, 'My Way', 1617202500)
 * ```
 */
class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}
