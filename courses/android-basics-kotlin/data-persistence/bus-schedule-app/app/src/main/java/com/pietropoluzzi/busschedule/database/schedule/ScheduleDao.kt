/*
 * Copyright (C) 2023 Pietro Poluzzi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.busschedule.database.schedule

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Query
import androidx.room.Room
import kotlinx.coroutines.flow.Flow

/**
 * [Dao] stands for **Data Access Object**.
 * A class marked with `@Dao` annotation defines the database interactions.
 * The class marked with `@Dao` should either be an interface or an abstract class.
 * At compile time, [Room] will generate an implementation of the class when it is referenced by a [Database].
 *
 * Provides access to read/write operations on the schedule table.
 * Used by the view models to format the query results for use in the UI.
 *
 * In the below functions, wrapping `<List<Schedule>>` with a [Flow] allows the DAO to continuously emit data from the database.
 * If an item is inserted, updated, or deleted, the result will be sent back to the fragment.
 */
@Dao
interface ScheduleDao {

    /**
     * The value of the `@Query` annotation includes the actual query that will be run when this method is called.
     * This query is verified at compile time by [Room] to ensure that it compiles fine against the database.
     * [Flow] an asynchronous data stream that sequentially emits values and completes normally or with an exception.
     * @return a [Flow] of `<List<Schedule>>`
     */
    @Query("SELECT * FROM schedule ORDER BY arrival_time ASC")
    fun getAll(): Flow<List<Schedule>>

    /**
     * Reference Kotlin values from the query by preceding it with a colon, like `:stopName`
     * @param stopName
     * @return a [Flow] of `<List<Schedule>>`
     */
    @Query("SELECT * FROM schedule WHERE stop_name = :stopName ORDER BY arrival_time ASC")
    fun getByStopName(stopName: String): Flow<List<Schedule>>

}
