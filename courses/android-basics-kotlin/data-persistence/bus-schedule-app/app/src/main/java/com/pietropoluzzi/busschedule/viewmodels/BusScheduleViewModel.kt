/*
 * Copyright (C) 2023 Pietro Poluzzi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.busschedule.viewmodels

import androidx.lifecycle.ViewModel
import com.pietropoluzzi.busschedule.database.schedule.Schedule
import com.pietropoluzzi.busschedule.database.schedule.ScheduleDao
import kotlinx.coroutines.flow.Flow

/**
 * The [ViewModel] is meant to be lifecycle aware, it should be instantiated by an object that can respond to lifecycle events.
 * If you instantiate it directly in a fragment, then the fragment object will have to handle everything, including all the memory management.
 * Instead, create a *factory* class that will instantiate view model objects for you.
 */
class BusScheduleViewModel(private val scheduleDao: ScheduleDao): ViewModel() {

    fun fullSchedule(): Flow<List<Schedule>> = scheduleDao.getAll()

    fun scheduleForStopName(name: String): Flow<List<Schedule>> = scheduleDao.getByStopName(name)
}