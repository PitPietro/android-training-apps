/*
 * Copyright (C) 2023 Pietro Poluzzi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.busschedule.database.schedule

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Represents a single table in the database.
 * Each row is a separate instance of the [Schedule] data class.
 * Each property corresponds to a column.
 * Additionally, an ID is needed as a unique identifier for each row in the database.
 *
 *  SQL types used in the database are `INTEGER` for `Int` and `TEXT` for `String`.
 *  Mapping the model class' data types to the database ones is automatically handled by `Room`.
 */
@Entity
data class Schedule(
    /**
     * Primary Key of the [Schedule] entity
     */
    @PrimaryKey val id: Int,
    /**
     * Do not use `@NonNull` annotation in Kotlin: the nullability is already implied by the `String` type not ending with `?`
     */
    @ColumnInfo(name = "stop_name") val stopName: String,
    /**
     * Do not use `@NonNull` annotation in Kotlin: the nullability is already implied by the `Int` type not ending with `?`.
     * [UNIX timestamp](https://www.unixtimestamp.com/) time, to be converted into a `Date` object
     */
    @ColumnInfo(name = "arrival_time") val arrivalTime: Int
)
