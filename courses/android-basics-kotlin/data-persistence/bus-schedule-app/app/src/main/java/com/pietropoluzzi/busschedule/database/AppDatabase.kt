/*
 * Copyright (C) 2023 Pietro Poluzzi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pietropoluzzi.busschedule.database

import android.app.Application
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pietropoluzzi.busschedule.database.schedule.Schedule
import com.pietropoluzzi.busschedule.database.schedule.ScheduleDao

/**
 * This class mainly:
 * - defines a database and specifies its data tables
 * - provides access to a single instance of each `DAO` class
 * - pre-populates the database
 *
 * Version is incremented as new tables or columns are changed.
 * The app checks the version with the one in the database to determine if and how a migration should be performed.
 * The `companion object` is used to prevent multiple instances of the database to exist.
 * `companion object` is the Kotlin way to say something is *static*.
 */
@Database(entities = [Schedule::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun scheduleDao(): ScheduleDao

    companion object {
        /**
         * [Volatile](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.jvm/-volatile/) annotation class.
         */
        @Volatile
        private var INSTANCE: AppDatabase? = null

        /**
         * Get `DB` file from path: `app/src/main/assets/database/bus_schedule.db` to populate the app's local database.
         * Use the Elvis operator to either return the existing instance of the database (if it already exists) or create it for the first time if needed.
         * @param context [Application] context
         * @return
         */
        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "app_database")
                    .createFromAsset("database/bus_schedule.db")
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}
