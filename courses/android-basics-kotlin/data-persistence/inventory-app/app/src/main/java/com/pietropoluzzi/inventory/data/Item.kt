/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.inventory.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.NumberFormat

/**
 * Represent a database entity.
 * To ensure consistency and meaningful behavior, [data classes](https://kotlinlang.org/docs/data-classes.html)
 * have to fulfill the following requirements:
 * - The primary constructor needs to have at least one parameter
 * - All primary constructor parameters need to be marked as `val` or `var`
 * - Data classes cannot be `abstract`, `open`, `sealed` or `inner`
 *
 * [Entity](https://developer.android.com/reference/androidx/room/Entity) class defines a table.
 * Each instance of this class represents a row in the database table.
 * The entity class has mappings to tell Room how it intends to present and interact with the information in the database.
 * `@Entity` annotation marks a class as a database Entity class.
 * A database table is created for each Entity class.
 * Each field of the Entity is represented as a column in the database, unless it is denoted otherwise.
 * Use `tableName` parameter to specify a different name to the database.
 */
@Entity(tableName = "item")
data class Item(
    /**
     * With `autoGenerate` set to `true`, SQLite generate the unique identifier.
     */
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    /**
     * `ColumnInfo` annotation is used to customise the column associated with the particular field.
     */
    @ColumnInfo(name = "name")
    val itemName: String,

    @ColumnInfo(name = "price")
    val itemPrice: Double,

    @ColumnInfo(name = "quantity")
    val quantityInStock: Int
)

/**
 * # Extension Functions
 * Kotlin provides an ability to extend a class with new functionality without having to inherit from the class or modify the existing definition of the class.
 * You can add functions to an existing class without having to access its source code.
 * This is done via special declarations called *extensions*.
 * You can write new functions for a class from a third-party library that you can't modify.
 * Such functions are available for calling in the usual way, as if they were methods of the original class.
 * These functions are called extension functions.
 * There are also extension properties that let you define new properties for existing classes.
 * Extension functions don't actually modify the class, but allow you to use the dot-notation when calling the function on objects of that class.
 *
 * @return formatted price as String
 */
fun Item.getFormattedPrice(): String = NumberFormat.getCurrencyInstance().format(itemPrice)