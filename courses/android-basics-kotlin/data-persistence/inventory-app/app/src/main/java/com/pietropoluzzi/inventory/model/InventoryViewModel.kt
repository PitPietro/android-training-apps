/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.inventory.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.pietropoluzzi.inventory.data.Item
import com.pietropoluzzi.inventory.data.ItemDao
import kotlinx.coroutines.launch

class InventoryViewModel(private val itemDao: ItemDao) : ViewModel() {
    /**
     * Cache all items form the database using [LiveData].
     * [asLiveData] creates a [LiveData] that has values collected from the origin `Flow`.
     */
    val allItems: LiveData<List<Item>> = itemDao.getItems().asLiveData()

    /**
     * Tells if the given [item] is available in the stock.
     *
     * @param item [Item] instance to check
     * @return `True` is stock is available to sell, `False` otherwise
     */
    fun isStockAvailable(item: Item): Boolean {
        return (item.quantityInStock > 0)
    }

    /**
     * Updates an existing Item in the database.
     *
     * @param itemId item's ID
     * @param itemName item's name
     * @param itemPrice item's price
     * @param itemCount item's count
     */
    fun updateItem(
        itemId: Int,
        itemName: String,
        itemPrice: String,
        itemCount: String
    ) {
        val updatedItem = getUpdatedItemEntry(itemId, itemName, itemPrice, itemCount)
        updateItem(updatedItem)
    }

    /**
     * Launching a new coroutine to update an item in a non-blocking way
     *
     * @param item [Item] instance to update
     */
    private fun updateItem(item: Item) {
        viewModelScope.launch {
            itemDao.update(item)
        }
    }

    /**
     * Decreases the stock by one unit and updates the database.
     *
     * Use the [copy](https://kotlinlang.org/docs/data-classes.html#copying) function to copy an object.
     * It allows you to alter some of its properties while keeping the rest unchanged.
     *
     * @param item [Item] instance to sell
     */
    fun sellItem(item: Item) {
        if (item.quantityInStock > 0) {
            // decrease the quantity by 1
            val newItem = item.copy(quantityInStock = item.quantityInStock - 1)
            updateItem(newItem)
        }
    }

    /**
     * Adds the data to the database in a non-blocking way.
     * To interact with the database off the main thread, start a coroutine and call the DAO method within it.
     *
     * The [viewModelScope] is an extension property to the [ViewModel] class.
     * It automatically cancels its child coroutines when the [ViewModel] is destroyed.
     *
     * Suspend functions are only allowed to be called from a coroutine or another suspend function.
     *
     * @param item [Item] instance to add
     */
    private fun insertItem(item: Item) {
        // start a coroutine
        viewModelScope.launch {
            itemDao.insert(item)
        }
    }

    /**
     * Launching a new coroutine to delete an item in a non-blocking way.
     *
     * @param item [Item] instance to delete
     */
    fun deleteItem(item: Item) {
        viewModelScope.launch {
            itemDao.delete(item)
        }
    }

    /**
     * Retrieve an item from the repository using its ID.
     *
     * @param id identifier of the [Item] instance to retrieve
     * @return [LiveData] of the retrieved [Item]
     */
    fun retrieveItem(id: Int): LiveData<Item> {
        return itemDao.getItem(id).asLiveData()
    }

    /**
     * Get new [Item] entry starting from its values.
     * Parameters are all string that will the converted before creating the [Item] instance.
     *
     * @param itemName item's name
     * @param itemPrice item's price
     * @param itemCount item's count
     * @return populated [Item] entry
     */
    private fun getNewItemEntry(itemName: String, itemPrice: String, itemCount: String): Item {
        // apply casts from String to Double and Int and return an Item instance
        return Item(
            itemName = itemName,
            itemPrice = itemPrice.toDouble(),
            quantityInStock = itemCount.toInt()
        )
    }

    /**
     * Adds a new [Item] to the database.
     *
     * @param itemName item's name
     * @param itemPrice item's price
     * @param itemCount item's count
     */
    fun addNewItem(itemName: String, itemPrice: String, itemCount: String) {
        val newItem = getNewItemEntry(itemName, itemPrice, itemCount)
        insertItem(newItem)
    }

    /**
     * Checks if an entry is valid: none of the field is blank.
     * Verify user input before adding or updating the entity in the database.
     * This validation needs to be done in the [ViewModel] and not in the Fragment.
     *
     * @param itemName
     * @param itemPrice
     * @param itemCount
     * @return
     */
    fun isEntryValid(itemName: String, itemPrice: String, itemCount: String): Boolean {
        if (itemName.isBlank() || itemPrice.isBlank() || itemCount.isBlank()) {
            return false
        }
        return true
    }

    /**
     * Called to update an existing entry in the Inventory database.
     *
     * @param itemId
     * @param itemName
     * @param itemPrice
     * @param itemCount
     * @return instance of the [Item] entity class with the item info updated by the user
     */
    private fun getUpdatedItemEntry(
        itemId: Int,
        itemName: String,
        itemPrice: String,
        itemCount: String
    ): Item {
        return Item(
            id = itemId,
            itemName = itemName,
            itemPrice = itemPrice.toDouble(),
            quantityInStock = itemCount.toInt()
        )
    }
}