/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.inventory.data

import android.content.Context
import androidx.room.Room
import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * The [Database] class provides the app with instances of the DAOs.
 * The app can use the DAOs to retrieve data from the database as instances of the associated data entity objects.
 * The app can also use the defined data entities to update rows from the corresponding tables, or to create new rows for insertion.
 * This class has one method that either creates an instance of the [RoomDatabase] if it doesn't exist, or returns the existing instance.
 *
 * General process for getting the [RoomDatabase] instance:
 * - Create a `public abstract` class that extends [RoomDatabase].
 *   The new abstract class you defined acts as a database holder.
 *   The class is `abstract` because [Room] creates the implementation for you.
 * - Annotate the class with `@Database` where to list the entities for the database and set the version number.
 * - Define an `abstract` method or property that returns an [ItemDao] instance and the [Room] will generate the implementation for you.
 * - Make the [RoomDatabase] a singleton.
 * - Use [Room]'s [Room.databaseBuilder] to create the database (`item_database`) only if it doesn't exist.
 *   Otherwise, return the existing database.
 *
 * @constructor Create empty Item room database
 */
@Database(entities = [Item::class], version = 1, exportSchema = false)
abstract class ItemRoomDatabase : RoomDatabase() {

    /**
     * The database needs to know about the DAO
     *
     * @return [ItemDao] instance
     */
    abstract fun itemDao(): ItemDao

    companion object  {

        /**
         * Keeps a reference to the database, when one has been created (initialized to `null`).
         * The value of a *volatile* variable will never be cached, and all writes and reads will be done to and from the main memory.
         * This helps make sure the value of `INSTANCE` is always up-to-date and the same for all execution threads.
         * It means that changes made by one thread to `INSTANCE` are visible to all other threads immediately.
         */
        @Volatile
        private var INSTANCE: ItemRoomDatabase? = null

        /**
         * Multiple threads can potentially run into a race condition and ask for a database instance at the same time.
         * Wrapping the code inside a `synchronized` block means that only one thread of execution at a time can enter this block of code.
         * It makes sure the database only gets initialized once.
         *
         * You would have to provide a migration object with a migration strategy for when the schema changes.
         * A migration object is an object that defines how you take all rows with the old schema and convert them to rows in the new schema, so that no data is lost.
         *
         * @param context Application context
         * @return
         */
        fun getDatabase(context: Context): ItemRoomDatabase {
            // use the elvis operator ?:
            return INSTANCE ?: synchronized(this) {
                // initialize INSTANCE if null
                // use the database builder to get the database
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ItemRoomDatabase::class.java,
                    "item_database"
                )
                    .fallbackToDestructiveMigration() // add migration strategy
                    .build()

                INSTANCE = instance
                return instance
            }
        }
    }
}