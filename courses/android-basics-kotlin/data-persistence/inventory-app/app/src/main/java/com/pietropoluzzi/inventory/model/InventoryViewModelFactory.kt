/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.inventory.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pietropoluzzi.inventory.data.ItemDao

class InventoryViewModelFactory(private val itemDao: ItemDao) : ViewModelProvider.Factory {

    /**
     * Check if the [modelClass] is the same as the [InventoryViewModel] class and return an instance of it. Otherwise, throw an exception.
     *
     * @param T type of the custom [ViewModel]
     * @param modelClass model to check
     * @return an [InventoryViewModel] instance
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InventoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return InventoryViewModel(itemDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}