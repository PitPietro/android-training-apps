/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.inventory.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

/**
 * # Item DAO
 * The Data Access Object pattern separates the **persistence layer** with the rest of the application.
 * This isolation follows the **single responsibility principle**.
 * This allows the **data access layer** to be changed independently of the code that uses the data.
 * For common database operations, Room provides convenience annotations: `@Insert`, `@Delete`, and `@Update`.
 * For everything else, use the `@Query` annotation to write any query that's supported by SQLite.
 *
 * As you write your queries in Android Studio, the compiler checks your SQL queries for syntax errors.
 *
 * For the inventory app, you need to be able to do the following:
 * - Insert or add a new item,
 * - Update an existing item to update name, price, and quantity,
 * - Get a specific item based on its primary key,
 * - Get all items, so you can display them,
 * - Delete an entry in the database.
 */
@Dao
interface ItemDao {

    /**
     * The database operations can take a long time to execute, so they should run on a separate thread.
     * Fhe function `suspend` since it's being called from a coroutine.
     *
     * `OnConflict` argument tells `Room` what to do in case of a conflict.
     * In this case, it ignores a new item if it's primary key is already in the database.
     *
     * Note: the function could have be named in a different way: the important part is the annotation.
     *
     * @param item instance of the `Entity` class [Item]
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(item: Item)

    /**
     * The updated entity has the same key as the entity that's passed in.
     * You can update some or all of the entity's other properties.
     *
     * @param item instance of the `Entity` class [Item]
     */
    @Update
    suspend fun update(item: Item)

    /**
     * The `@Delete` annotation deletes one or more items.
     * You need to pass the entity(s) to be deleted.
     * If you don't have the entity, you'd have to fetch it before calling this function.
     * @param item
     */
    @Delete
    suspend fun delete(item: Item)

    /**
     * Supply the query to run as a string parameter to the `@Query` annotation.
     * Select all columns from the item where the `id` matches the `id` argument.
     * Notice the colon before `id`.
     * Use the colon notation in the query to reference arguments in the function.
     *
     * [Flow] an asynchronous data stream that sequentially emits values and completes normally or with an exception.
     * [Room] also runs the query on the background thread.
     * The function doesn't have to be marked as `suspend` and be called inside a coroutine scope.
     *
     * @param id identifier of the [Item] instance to get
     * @return a [Flow] of [Item]
     */
    @Query("SELECT * FROM item WHERE id = :id")
    fun getItem(id: Int): Flow<Item>

    /**
     * [Room] will keep this [Flow] updated: you only need to explicitly get the data once.
     *
     * @return a [Flow] of a [List] of [Item]
     */
    @Query("SELECT * FROM item ORDER BY name ASC")
    fun getItems(): Flow<List<Item>>
}