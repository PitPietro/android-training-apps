/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.repository_pattern.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.pietropoluzzi.repository_pattern.database.VideosDatabase
import com.pietropoluzzi.repository_pattern.database.asDomainModel
import com.pietropoluzzi.repository_pattern.domain.DevByteVideo
import com.pietropoluzzi.repository_pattern.network.DevByteNetwork
import com.pietropoluzzi.repository_pattern.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Repository for fetching *DevByte* videos from the network and storing them on disk.
 */
class VideosRepository(private val database: VideosDatabase) {

    val videos: LiveData<List<DevByteVideo>> = database.videoDao.getVideos().map {
        it.asDomainModel()
    }

    suspend fun refreshVideos() {
        withContext(Dispatchers.IO) {
            val playlist = DevByteNetwork.devByteService.getPlaylist()
            database.videoDao.insertAll(playlist.asDatabaseModel())
        }
    }

}

