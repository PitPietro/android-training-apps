/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.marsphotos

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pietropoluzzi.marsphotos.overview.OverviewFragment

/**
 * ## Introduction
 * Sets the content view `activity_main`, a fragment container that contains
 * [OverviewFragment].
 *
 * Most web servers today run web services using **REST**, which stands for REpresentational State Transfer.
 * Web services that offer this architecture are known as *RESTful services*.
 * Requests are made to **RESTful** web services in a standardized way via URIs (Uniform Resource Identifier).
 * An URI identifies a resource in the server by name, without implying its location or how to access it.
 * For example, in this app, you retrieve the image urls using [this](android-kotlin-fun-mars-server.appspot.com) server URI (This server hosts both Mars real-estate and Mars photos):
 * A Uniform Resource Locator (URL) is a URI that specifies the means of acting upon or obtaining the representation of a resource, i.e. specifying both its primary access mechanism and network location.
 *
 * For example, the following URL gets a list of:
 * 1. all available real estate properties on Mars! [/realestate](https://android-kotlin-fun-mars-server.appspot.com/realestate)
 * 2. Mars photos: [/photos](https://android-kotlin-fun-mars-server.appspot.com/photos)
 *
 * These URLs refer to a resource identified such as `/realestate` or `/photos`, that is obtainable via the **Hypertext Transfer Protocol** (`http:`) from the network.
 *
 * ## Summary
 * REST web services:
 * - A web service is software-based functionality offered over the internet that enables your app to make requests and get data back.
 * - To use a web service, an app must establish a network connection and communicate with the service. Then the app must receive and parse response data into a format the app can use.
 * - The Retrofit library is a client library that enables your app to make requests to a REST web service.
 * - Use converters to tell Retrofit what to do with data it sends to the web service and gets back from the web service.
 * For example, the ScalarsConverter converter treats the web service data as a String or other primitive.
 * - To enable your app to make connections to the internet, add the `"android.permission.INTERNET"` permission in the Android manifest.
 *
 * JSON parsing:
 * - The response from a web service is often formatted in JSON, a common format for representing structured data.
 * - A JSON object is a collection of key-value pairs.
 * - A collection of JSON objects is a JSON array. You get a JSON array as a response from a web service.
 * - The keys in a key-value pair are surrounded by quotes. The values can be numbers or strings.
 * - The Moshi library is an Android JSON parser that converts a JSON string into Kotlin objects. Retrofit has a converter that works with Moshi.
 * - Moshi matches the keys in a JSON response with properties in a data object that have the same name.
 * - To use a different property name for a key, annotate that property with the `@Json` annotation and the JSON key name.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}