/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.workmanager_sample_app.workers

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.text.TextUtils
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkRequest
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.pietropoluzzi.workmanager_sample_app.util.KEY_IMAGE_URI

private const val TAG = "BlurWorker"

/**
 * # Blur Worker
 * ## WorkManager Basics
 * There are a few [WorkManager] classes:
 * - [Worker]: The actual work you want to perform in the background.
 * Extend this class and override the `doWork()` method.
 * - [WorkRequest]: A request to do some work.
 * When making the WorkRequest you can also specify things like Constraints on when the Worker should run.
 * - [WorkManager]: Schedules the [WorkRequest] and makes it run.
 * It schedules WorkRequests in a way that spreads out the load on system resources, while honoring the constraints you specify.
 *
 * @param ctx [Context] instance
 * @param params
 */
class BlurWorker(ctx: Context, params: WorkerParameters) : Worker(ctx, params) {

    override fun doWork(): Result {
        val appContext = applicationContext

        val resourceUri = inputData.getString(KEY_IMAGE_URI)

        makeStatusNotification("Blurring image", appContext)

        // this is an utility function added to emulate slower work
        sleep()

        return try {
            if (TextUtils.isEmpty(resourceUri)) {
                Log.e(TAG, "Invalid input uri")
                throw IllegalArgumentException("Invalid input uri")
            }

            val resolver = appContext.contentResolver

            val picture = BitmapFactory.decodeStream(
                    resolver.openInputStream(Uri.parse(resourceUri)))

            val output = blurBitmap(picture, appContext)

            // write bitmap to a temp file
            val outputUri = writeBitmapToFile(appContext, output)

            // provide the Output URI as an output Data to make this temporary image
            // easily accessible to other workers for further operations
            val outputData = workDataOf(KEY_IMAGE_URI to outputUri.toString())

            Result.success(outputData)
        } catch (throwable: Throwable) {
            Log.e(TAG, "Error applying blur")
            throwable.printStackTrace()
            Result.failure()
        }
    }
}
