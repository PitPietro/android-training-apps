/*
 * Copyright (C) 2023 Pietro Poluzzi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pietropoluzzi.workmanager_sample_app.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.WorkInfo
import com.pietropoluzzi.workmanager_sample_app.model.BlurViewModel
import com.pietropoluzzi.workmanager_sample_app.model.BlurViewModelFactory
import com.pietropoluzzi.workmanager_sample_app.util.KEY_IMAGE_URI
import com.pietropoluzzi.workmanager_sample_app.R
import com.pietropoluzzi.workmanager_sample_app.databinding.ActivityBlurBinding

/**
 * # Blur Activity
 *
 * ## What is a WorkManager
 * [WorkManager](https://developer.android.com/topic/libraries/architecture/workmanager) is part of **Android Jetpack**.
 * It's an [Architecture Component](https://developer.android.com/topic/architecture) for
 * background work that needs a combination of opportunistic and guaranteed execution.
 * Opportunistic execution means that WorkManager will do the background work as soon as it can.
 * Guaranteed execution means that it will take care of the logic to start the work under a variety
 * of situations, even if the user navigates away from the app.
 *
 * WorkManager is a very flexible library that, above others, include these benefits:
 * - Support for both asynchronous one-off and periodic tasks
 * - Support for constraints such as network conditions, storage space, and charging status
 * - Chaining of complex work requests, including running work in parallel
 * - Output from one work request used as input for the next
 * - Handling API level compatibility back to API level 14
 * - Working with or without Google Play services
 * - Following system health best practices
 * - LiveData support to easily display work request state in UI
 *
 * Note: WorkManager sits on top of a few APIs such as `JobScheduler` and `AlarmManager`.
 * WorkManager picks the right APIs to use, based on conditions like the user's device API level.
 *
 * ## When to use WorkManager
 * The WorkManager library is a good choice for tasks that are useful to complete.
 * Some examples of tasks that are a good use of WorkManager:
 * - Uploading logs
 * - Applying filters to images and saving the image
 * - Periodically syncing local data with the network
 *
 * WorkManager offers guaranteed execution, and not all tasks require that.
 * As such, it is not a catch-all for running every task off of the main thread.
 * Check out [background processing](https://developer.android.com/guide/background/) guide.
 */
class BlurActivity : AppCompatActivity() {

    private val viewModel: BlurViewModel by viewModels { BlurViewModelFactory(application) }
    private lateinit var binding: ActivityBlurBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBlurBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.goButton.setOnClickListener { viewModel.applyBlur(blurLevel) }

        // setup view output image file button
        binding.seeFileButton.setOnClickListener {
            viewModel.outputUri?.let { currentUri ->
                val actionView = Intent(Intent.ACTION_VIEW, currentUri)
                actionView.resolveActivity(packageManager)?.run {
                    startActivity(actionView)
                }
            }
        }

        // hookup the Cancel button
        binding.cancelButton.setOnClickListener { viewModel.cancelWork() }

        viewModel.outputWorkInfos.observe(this, workInfosObserver())
    }

    private fun workInfosObserver(): Observer<List<WorkInfo>> {
        return Observer { listOfWorkInfo ->

            // grab a single WorkInfo if it exists
            // this code could be in a Transformation in the ViewModel
            // they are included here so that the entire process of displaying a WorkInfo is in one location.

            // if there are no matching work info, do nothing
            if (listOfWorkInfo.isEmpty()) {
                return@Observer
            }

            // only care about the one output status
            // every continuation has only one worker tagged TAG_OUTPUT
            val workInfo = listOfWorkInfo[0]

            if (workInfo.state.isFinished) {
                showWorkFinished()

                // this processing would be in the ViewModel
                // it is not directly related to drawing views on screen
                val outputImageUri = workInfo.outputData.getString(KEY_IMAGE_URI)

                // if there is an output file show "See File" button
                if (!outputImageUri.isNullOrEmpty()) {
                    viewModel.setOutputUri(outputImageUri)
                    binding.seeFileButton.visibility = View.VISIBLE
                }
            } else {
                showWorkInProgress()
            }
        }
    }

    /**
     * Shows and hides views for when the Activity is processing an image.
     */
    private fun showWorkInProgress() {
        with(binding) {
            progressBar.visibility = View.VISIBLE
            cancelButton.visibility = View.VISIBLE
            goButton.visibility = View.GONE
            seeFileButton.visibility = View.GONE
        }
    }

    /**
     * Shows and hides views for when the Activity is done processing an image.
     */
    private fun showWorkFinished() {
        with(binding) {
            progressBar.visibility = View.GONE
            cancelButton.visibility = View.GONE
            goButton.visibility = View.VISIBLE
        }
    }

    private val blurLevel: Int
        get() =
            when (binding.radioBlurGroup.checkedRadioButtonId) {
                R.id.radio_blur_lv_1 -> 1
                R.id.radio_blur_lv_2 -> 2
                R.id.radio_blur_lv_3 -> 3
                else -> 1
            }
}
