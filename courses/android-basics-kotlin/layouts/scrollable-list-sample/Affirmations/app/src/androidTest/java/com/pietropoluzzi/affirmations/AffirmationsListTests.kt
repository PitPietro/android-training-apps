package com.pietropoluzzi.affirmations

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Affirmations list tests
 *
 * If the run device is in lock mode, and/or activity inactive, will trigger this error.
 * Make sure your device is on and app/test is able to run in foreground.
 *
 * @constructor Create empty Affirmations list tests
 */
@RunWith(AndroidJUnit4::class)
class AffirmationsListTests {

    @get:Rule
    val activity = ActivityScenarioRule(MainActivity::class.java)

    /**
     * # Scroll to Item
     *
     * Provide a [ViewInteraction] to perform an action on with [onView].
     * Use [withId] to make sure to pass in the `ID` of the [RecyclerView] that was used for the affirmations to [onView] method.
     * Call [ViewInteraction.perform] on the [ViewInteraction].
     *
     * When you are unsure of the length of your list you can use the [RecyclerViewActions.scrollTo] action.
     * It requires a `Matcher<View!>!` to find a particular item (use [withText]).
     *
     * Make an assertion to ensure that the UI is displaying the expected information.
     * Make sure that once you have scrolled to the last item, the text associated with the final affirmation is displayed.
     * Pass in a [withText] *view matcher* to [ViewInteraction].
     * To that method, pass the string resource that contains the text of the last affirmation.
     * [withText] method identifies a UI component based on the text it displays.
     * For this component, all that needs to be done is to check that it displays the desired text: call [ViewInteraction.check] on the [ViewInteraction].
     * [ViewInteraction.check] requires a [ViewAssertion], for which you can use the [matches] method.
     * Finally, make the assertion that the UI component is displayed by passing the method [isDisplayed].
     *
     * Docs:
     * - [RecyclerViewActions](https://developer.android.com/reference/androidx/test/espresso/contrib/RecyclerViewActions)
     */
    @Test
    fun scroll_to_item() {
        onView(withId(R.id.recycler_view)).perform(
            RecyclerViewActions
                // .scrollTo<RecyclerView.ViewHolder>(withText(R.string.aff10))
                .scrollToPosition<RecyclerView.ViewHolder>(9)
        )

        onView(withText(R.string.aff10))
            .check(matches(isDisplayed()))
    }
}