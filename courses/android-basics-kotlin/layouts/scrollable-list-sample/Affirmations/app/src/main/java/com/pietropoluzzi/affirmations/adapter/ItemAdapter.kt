package com.pietropoluzzi.affirmations.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pietropoluzzi.affirmations.R
import com.pietropoluzzi.affirmations.model.Affirmation

/**
 * Item adapter
 *
 * @property context Activity context needed to resolve the string resources
 * @property dataset
 * @constructor Create empty Item adapter
 */
class ItemAdapter(private val context: Context, private val dataset: List<Affirmation>) :
    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    /**
     * Get item count
     * @return the size of the dataset (invoked by the layout manager)
     */
    override fun getItemCount() = dataset.size

    /**
     * Item view holder
     *
     * Provide a reference to the views for each data item.
     * Complex data items may need more than one view per item, and you provide access to all the views for a data item in a view holder.
     * Each data item is just an [Affirmation] object.
     *
     * @property view [R.layout.list_item] view
     * @constructor Create empty Item view holder
     */
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.item_image)
        val textView: TextView = view.findViewById(R.id.item_title)
    }

    /**
     * Create new views (invoked by the layout manager)
     *
     * @param parent The view group that the new list item view will be attached to as a child
     * @param viewType If there are different list item layouts displayed within the RecyclerView, there are different item view types.
     * [RecyclerView] can only recycle views with the same item view type.
     *
     * @return [ItemViewHolder] instance attached to [R.layout.list_item] layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        // create a new view
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)

        return ItemViewHolder(adapterLayout)
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     *
     * @param holder [ItemViewHolder] previously created by [onCreateViewHolder] method
     * @param position Current item position in the list
     */
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]

        // update the view referenced by the view holder to reflect the correct data for this item
        holder.textView.text = context.resources.getString(item.stringResId)
        holder.imageView.setImageResource(item.imageResId)
    }
}