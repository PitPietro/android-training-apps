package com.pietropoluzzi.affirmations.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

/**
 * Affirmation
 *
 * Since both parameters are integer values, the caller could accidentally pass in the arguments in the wrong order.
 * To avoid this, use Resource annotations: they add additional info to classes, methods or parameters.
 * Annotations are always declared with an `@` symbol.
 * You'll get a warning if you supply the wrong type of resource ID.
 *
 * Docs:
 * - [Kotlin Data Classes](https://kotlinlang.org/docs/data-classes.html)
 * - [Improve code inspection with annotations](https://developer.android.com/studio/write/annotations)
 *
 * @property stringResId resource `ID` for the text of the affirmation
 * @property imageResId resource `ID` for the image of the affirmation
 *
 * @constructor Create empty Affirmation
 */
data class Affirmation(
    @StringRes val stringResId: Int,
    @DrawableRes val imageResId: Int
)