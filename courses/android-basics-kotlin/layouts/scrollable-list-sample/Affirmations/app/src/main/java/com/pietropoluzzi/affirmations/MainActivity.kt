package com.pietropoluzzi.affirmations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pietropoluzzi.affirmations.adapter.ItemAdapter
import com.pietropoluzzi.affirmations.data.Datasource
import com.pietropoluzzi.affirmations.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // store the returned list of affirmations
        val myDataset = Datasource().loadAffirmations()

        // create a reference to view's recycler view
        val recyclerView = binding.recyclerView

        // assign the ItemAdapter object to the adapter property of recyclerView
        recyclerView.adapter = ItemAdapter(this, myDataset)

        // since layout size of the RecyclerView is fixed, it's possible to:
        recyclerView.setHasFixedSize(true)
    }
}