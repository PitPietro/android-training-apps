package com.pietropoluzzi.affirmations

import android.content.Context
import com.pietropoluzzi.affirmations.adapter.ItemAdapter
import com.pietropoluzzi.affirmations.model.Affirmation
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.mockito.Mockito.mock

class AffirmationAdapterTests {

    /**
     * Unit tests run on the **JVM** (Java Virtual Machine) and not on an actual device, so there is no [Context].
     * The [mock] method allows to create a "mocked" instance of a [Context].
     * It doesn't have any real functionality, but it can be used to test methods that require a context.
     */
    private val context = mock(Context::class.java)

    /**
     * Adapter Size
     *
     * This test checks that the size of the adapter is the size of the list that was passed to the adapter.
     *
     */
    @Test
    fun adapter_size() {
        val data = listOf(
            Affirmation(R.string.aff1, R.drawable.image1),
            Affirmation(R.string.aff2, R.drawable.image2)
        )

        // `getItemCount()` returns the size of the adapter
        val adapter = ItemAdapter(context, data)

        assertEquals("ItemAdapter has not the correct size", data.size, adapter.itemCount)
    }

}